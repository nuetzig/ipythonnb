# extract coordinates from svg file
#
# 1) opens file 'filename.svg'
# 2) read coordinates out of polygon classes
# <polygon class="fil0 str0" points="x1,y1 x2,y3 x2,y3 "/>
# store vertices and connection between them (python syntax) to textfile 'polygonized.txt'
#
# Michael Baumann
# baumann@imes.mavt.ethz.ch
# 17.11.2016

import re  # regular expression module
import numpy as np
import json # JavaScript Object Notation
import sys; sys.path.insert(0,'..') # add parent path
import smallestenclosingcircle

# define filenames
fileNameIn = 'chameleonBoundary'
fileExtensionIn = '.svg'

fileNameOut = fileNameIn
fileExtensionOut = '.txt'

# read input file and store to 'strIn'
with open( fileNameIn + fileExtensionIn ) as fileIn:  # 'r' for read is default
    strIn=fileIn.read()

# any backslash character ('\') must be escaped when passed to re.compile()
# In order to express a ('\\') in python, both backslash characters need to be excaped, i.e. ('\\\\')
# Therefore, raw string notation (r'...') is used in which backslash characters are not handled in a special way

# find header
pat = r'viewBox="(\d+)\s(\d+)\s(\d+)\s(\d+)"'
viewBoxString = re.findall(pat,strIn)
if len(viewBoxString)!=1: print('viewBoxString not found')
viewBox = [int(val) for val in viewBoxString[0]]

# find vertices of object and approximation (if exists)
# object itself and bounding object (1)
pat = r'<polygon.*?str0.*?points="(\d+,\d+(?:\s\d+,\d+){2,})\s"\/>' # str0 for object
patApprox = r'<polygon.*?str1.*?points="(\d+,\d+(?:\s\d+,\d+){2,})\s"\/>' # str1 for approximation
polygonsStr = re.findall(pat,strIn)
polygonsApproxStr = re.findall(patApprox,strIn)
if len(polygonsStr):
    print('%i polygons of object found' % len(polygonsStr))
else:
    print('no object found')
if len(polygonsApproxStr):
    print('%i polygons of approximation found' % len(polygonsApproxStr))
    cApprox = True
else:
    print('no approximation found')
    cApprox = False

polygons = [np.array([vertexStr.split(',') for vertexStr in polygonStr.split(' ')]).astype(int) for polygonStr in polygonsStr]
if cApprox:
    polygonsApprox = [np.array([vertexStr.split(',') for vertexStr in polygonStr.split(' ')]).astype(int) for polygonStr in polygonsApproxStr]

# rescale vertices
xmin,ymin,xmax,ymax = viewBox
scale = min([1/(xmax-xmin),1/(ymax-ymin)])
polygons=[(polygon-[xmin,ymax])*scale*[1,-1] for polygon in polygons]
if cApprox:
    polygonsApprox=[(polygon-[xmin,ymax])*scale*[1,-1] for polygon in polygonsApprox]

# calculate area and boundingcircle
area = 0
for polygon in polygons:
    x0 = polygon[:,0]
    x1 = np.roll(x0,-1)
    y0 = polygon[:,1]
    y1 = np.roll(y0,-1)
    area+=0.5*abs(np.dot(x0,y1)-np.dot(x1,y0)) # shoelace formula

uniqueVertexlist=[]
for polygon in polygons:
    for vertex in polygon.tolist():
        if vertex not in uniqueVertexlist:
            uniqueVertexlist += [vertex]

boundingCircle = smallestenclosingcircle.make_circle(uniqueVertexlist)
centerBC = [boundingCircle[i] for i in [0,1]]
radiusBC = boundingCircle[2]

if cApprox:
    areaApprox = 0
    for polygon in polygonsApprox:
        x0 = polygon[:, 0]
        x1 = np.roll(x0, -1)
        y0 = polygon[:, 1]
        y1 = np.roll(y0, -1)
        areaApprox += 0.5 * abs(np.dot(x0, y1) - np.dot(x1, y0))  # shoelace formula

    uniqueVertexlist = []
    for polygon in polygonsApprox:
        for vertex in polygon.tolist():
            if vertex not in uniqueVertexlist:
                uniqueVertexlist += [vertex]

    boundingCircleApprox = smallestenclosingcircle.make_circle(uniqueVertexlist)
    centerBCApprox = [boundingCircleApprox[i] for i in [0, 1]]
    radiusBCApprox = boundingCircleApprox[2]

# create dictionary and save to file
polygonizedObjectDict={}
polygonizedObjectDict['area'] = area
polygonizedObjectDict['radiusBC'] = radiusBC
polygonizedObjectDict['centerBC'] = centerBC
polygonizedObjectDict['polygons'] = [polygon.tolist() for polygon in polygons]
#polygons0 = np.reshape(np.asarray(polygons0),(-1,3,2)) # reshape
if cApprox:
    polygonizedObjectDict['areaApprox'] = areaApprox
    polygonizedObjectDict['radiusBCApprox'] = radiusBCApprox
    polygonizedObjectDict['centerBCApprox'] = centerBCApprox
    polygonizedObjectDict['polygonsApprox'] = [polygon.tolist() for polygon in polygonsApprox]

with open(fileNameOut+fileExtensionOut,'w') as fileOut:  # 'w' for write (deletes existing file)
    json.dump(polygonizedObjectDict, fileOut,indent=0)

# load dictionary from file:
# with open( fileNameOut+fileExtensionOut ) as file:  # 'r' for read is default
#    polygonizedObjectDict = json.load(file)