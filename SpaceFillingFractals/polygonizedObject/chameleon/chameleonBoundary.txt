{
"radiusBCApprox": 0.6201427314684735,
"centerBC": [
0.5,
0.3691735389028326
],
"areaApprox": 0.7349228889399413,
"area": 0.44489454920157684,
"radiusBC": 0.6201427314684735,
"centerBCApprox": [
0.5,
0.3691735389028326
],
"polygonsApprox": [
[
[
0.9990139835066333,
0.7373610613122984
],
[
0.9990139835066333,
0.0009860164933667983
],
[
0.0009860164933667983,
0.0009860164933667983
],
[
0.0009860164933667983,
0.7373610613122984
]
]
],
"polygons": [
[
[
0.0009860164933667983,
0.7373610613122984
],
[
0.016672642524202225,
0.2702581570455361
],
[
0.0009860164933667983,
0.1918250268913589
]
],
[
[
0.9990139835066333,
0.7373610613122984
],
[
0.9494442452491934,
0.6262997490139836
],
[
0.9990139835066333,
0.5363929723915383
]
],
[
[
0.12916816063105058,
0.07502689135891001
],
[
0.09152025815704554,
0.11643958408031553
],
[
0.07959842237361062,
0.16896737181785587
],
[
0.15561133022588744,
0.09107206884187882
],
[
0.18259232699892436,
0.05539619935460739
]
],
[
[
0.14736464682681966,
0.14225528863391898
],
[
0.09107206884187882,
0.21656507708856224
],
[
0.07959842237361062,
0.16896737181785587
],
[
0.15561133022588744,
0.09107206884187882
]
],
[
[
0.09107206884187882,
0.21656507708856224
],
[
0.11545356758694873,
0.26470060953746866
],
[
0.1676228038723557,
0.3041412692721406
],
[
0.22705270706346362,
0.3305844388669774
],
[
0.295177482968806,
0.3519182502689136
],
[
0.2606669057009681,
0.31713875941197567
]
],
[
[
0.14736464682681966,
0.14225528863391898
],
[
0.09107206884187882,
0.21656507708856224
],
[
0.18160631050555756,
0.1676228038723557
]
],
[
[
0.2606669057009681,
0.31713875941197567
],
[
0.09107206884187882,
0.21656507708856224
],
[
0.18160631050555756,
0.1676228038723557
],
[
0.24094657583363213,
0.1789171746145572
],
[
0.31435998565794193,
0.2707063463607028
]
],
[
[
0.3467192542129796,
0.3709214772319828
],
[
0.3219792040157763,
0.3310326281821442
],
[
0.34806382215847975,
0.30333452850484044
]
],
[
[
0.3526353531731804,
0.28755826461097167
],
[
0.3219792040157763,
0.3310326281821442
],
[
0.34806382215847975,
0.30333452850484044
],
[
0.37513445679455004,
0.25681247759053427
]
],
[
[
0.3526353531731804,
0.28755826461097167
],
[
0.3072785944783077,
0.2885442811043385
],
[
0.31435998565794193,
0.2707063463607028
],
[
0.37513445679455004,
0.25681247759053427
]
],
[
[
0.4435281462889925,
0.2702581570455361
],
[
0.39888849049838654,
0.2643420580853353
],
[
0.42506274650412335,
0.25681247759053427
]
],
[
[
0.7064359985657942,
0.3183040516314091
],
[
0.6623341699533883,
0.3219792040157763
],
[
0.6837576192183579,
0.3044101828612406
]
],
[
[
0.6686088203657226,
0.3768375761921836
],
[
0.632126210111151,
0.4057009680889208
],
[
0.6857296522050915,
0.3281642165650771
],
[
0.7064359985657942,
0.3183040516314091
]
],
[
[
0.786572248117605,
0.3560415919684475
],
[
0.7492828970957333,
0.37316242380781645
],
[
0.7785048404446039,
0.3479741842954464
]
],
[
[
0.7179096450340624,
0.44245249193259234
],
[
0.6778415202581571,
0.42703477949085694
],
[
0.7082287558264612,
0.39342058085335246
],
[
0.7533166009322338,
0.3786303334528505
],
[
0.8131946934385085,
0.4441556113302259
],
[
0.764790247400502,
0.45132664037289355
]
],
[
[
0.016672642524202225,
0.2702581570455361
],
[
0.053334528504840446,
0.32941914664754396
],
[
0.0009860164933667983,
0.7373610613122984
]
],
[
[
0.0009860164933667983,
0.7373610613122984
],
[
0.21342775188239513,
0.5468806023664396
],
[
0.18295087845105773,
0.4795625672283973
],
[
0.14144854786661887,
0.4179813553244891
],
[
0.10451774829688061,
0.38006453926138406
],
[
0.05306561491574041,
0.3295087845105773
]
],
[
[
0.0009860164933667983,
0.0009860164933667983
],
[
0.08614198637504482,
0.02984940839010398
],
[
0.142524202223019,
0.0009860164933667983
]
],
[
[
0.0009860164933667983,
0.0009860164933667983
],
[
0.08614198637504482,
0.02984940839010398
],
[
0.05405163140910721,
0.06534600215130872
]
],
[
[
0.015776263893868773,
0.11921835783434924
],
[
0.054320544998207244,
0.06543564001434206
],
[
0.0009860164933667983,
0.0009860164933667983
]
],
[
[
0.0009860164933667983,
0.0009860164933667983
],
[
0.015776263893868773,
0.11921835783434924
],
[
0.0009860164933667983,
0.1918250268913589
]
],
[
[
0.7653280745787021,
0.7043743277160273
],
[
0.6997131588382933,
0.6948727142344927
],
[
0.7488347077805666,
0.6426138400860524
]
],
[
[
0.7653280745787021,
0.7043743277160273
],
[
0.6997131588382933,
0.6948727142344927
],
[
0.6342775188239513,
0.7214951595553962
],
[
0.7785048404446039,
0.7205091430620294
]
],
[
[
0.9990139835066333,
0.7373610613122984
],
[
0.5555754750806741,
0.7366439584080317
],
[
0.6342775188239513,
0.7214951595553962
],
[
0.7785048404446039,
0.7205091430620294
]
],
[
[
0.9990139835066333,
0.7373610613122984
],
[
0.8207242739333095,
0.7136966654714952
],
[
0.7785048404446039,
0.7205091430620294
]
],
[
[
0.9990139835066333,
0.7373610613122984
],
[
0.8207242739333095,
0.7136966654714952
],
[
0.8813194693438509,
0.6662782359268555
]
],
[
[
0.9990139835066333,
0.7373610613122984
],
[
0.9494442452491934,
0.6262997490139836
],
[
0.89951595553962,
0.6401936177841521
],
[
0.8813194693438509,
0.6662782359268555
]
],
[
[
0.9866439584080317,
0.5065435640014342
],
[
0.9990139835066333,
0.5363929723915383
],
[
0.9990139835066333,
0.28603442093940484
]
],
[
[
0.9866439584080317,
0.5065435640014342
],
[
0.9379705987809251,
0.47785944783076373
],
[
0.9990139835066333,
0.28603442093940484
]
],
[
[
0.9866439584080317,
0.5065435640014342
],
[
0.9379705987809251,
0.47785944783076373
],
[
0.9990139835066333,
0.28603442093940484
]
],
[
[
0.6686088203657226,
0.3768375761921836
],
[
0.7394227321620653,
0.359806382215848
],
[
0.7785048404446039,
0.3479741842954464
],
[
0.9990139835066333,
0.0009860164933667983
],
[
0.7064359985657942,
0.3183040516314091
]
],
[
[
0.9990139835066333,
0.28603442093940484
],
[
0.786572248117605,
0.3560415919684475
],
[
0.7533166009322338,
0.3786303334528505
],
[
0.8131946934385085,
0.4441556113302259
],
[
0.876389386877017,
0.4579598422373611
]
],
[
[
0.786572248117605,
0.3560415919684475
],
[
0.7785048404446039,
0.3479741842954464
],
[
0.9990139835066333,
0.0009860164933667983
],
[
0.9990139835066333,
0.28603442093940484
]
],
[
[
0.876389386877017,
0.4579598422373611
],
[
0.9379705987809251,
0.47785944783076373
],
[
0.9990139835066333,
0.28603442093940484
]
],
[
[
0.9990139835066333,
0.0009860164933667983
],
[
0.2026712083183937,
0.0014342058085335247
],
[
0.2505378271782001,
0.019361778415202584
]
],
[
[
0.9990139835066333,
0.0009860164933667983
],
[
0.27922194334887057,
0.0552169236285407
],
[
0.2505378271782001,
0.019361778415202584
]
],
[
[
0.9990139835066333,
0.0009860164933667983
],
[
0.27922194334887057,
0.0552169236285407
],
[
0.2947292936536393,
0.09645034062387953
]
],
[
[
0.37513445679455004,
0.25681247759053427
],
[
0.2800286841161707,
0.14064180709931876
],
[
0.24094657583363213,
0.1789171746145572
],
[
0.31435998565794193,
0.2707063463607028
]
],
[
[
0.9990139835066333,
0.0009860164933667983
],
[
0.42497310864109,
0.25672283972750093
],
[
0.37513445679455004,
0.25681247759053427
],
[
0.28074578702043745,
0.1405521692362854
],
[
0.2947292936536393,
0.09645034062387953
]
],
[
[
0.7064359985657942,
0.3183040516314091
],
[
0.9990139835066333,
0.0009860164933667983
],
[
0.6837576192183579,
0.3044101828612406
]
],
[
[
0.42470419505199,
0.25681247759053427
],
[
0.9990139835066333,
0.0009860164933667983
],
[
0.6837576192183579,
0.3044101828612406
],
[
0.6456615274291861,
0.31749731086410904
],
[
0.44361778415202585,
0.27043743277160276
]
],
[
[
0.41851918250268916,
0.2906955898171388
],
[
0.4048045894585873,
0.3356041591968448
],
[
0.41116887773395483,
0.36392972391538186
],
[
0.5207063463607028,
0.3750448189315167
],
[
0.59994621728218,
0.3560415919684475
],
[
0.6456615274291861,
0.31749731086410904
],
[
0.44361778415202585,
0.27043743277160276
]
],
[
[
0.5207063463607028,
0.3750448189315167
],
[
0.59994621728218,
0.3560415919684475
],
[
0.5759232699892435,
0.3944065973467193
]
],
[
[
0.0009860164933667983,
0.7373610613122984
],
[
0.39655790605951957,
0.7091251344567946
],
[
0.47992111868053067,
0.7373610613122984
]
],
[
[
0.0009860164933667983,
0.7373610613122984
],
[
0.39655790605951957,
0.7091251344567946
],
[
0.3209035496593761,
0.6686984582287558
]
],
[
[
0.0009860164933667983,
0.7373610613122984
],
[
0.2663140910720689,
0.619935460738616
],
[
0.3209035496593761,
0.6686984582287558
]
],
[
[
0.0009860164933667983,
0.7373610613122984
],
[
0.2663140910720689,
0.619935460738616
],
[
0.21342775188239513,
0.5468806023664396
]
],
[
[
0.22508067407673002,
0.14225528863391898
],
[
0.18160631050555756,
0.1420760129078523
],
[
0.1751523843671567,
0.10326281821441377
],
[
0.212352097525995,
0.09089279311581212
],
[
0.2329688060236644,
0.11652922194334887
]
]
]
}