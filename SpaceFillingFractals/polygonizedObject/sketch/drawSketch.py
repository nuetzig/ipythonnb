# draw chameleon

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import patches
import random
import json # JavaScript Object Notation

# define filenames
fileNameIn = 'chameleon'
fileExtensionIn = '.txt'

with open( fileNameIn+fileExtensionIn ) as file:  # 'r' for read is default
    polygonizedObjectDict = json.load(file)

polygons = np.array(polygonizedObjectDict['polygons'])
area = polygonizedObjectDict['area']
radiusBC = polygonizedObjectDict['radiusBC']
centerBC = polygonizedObjectDict['centerBC']


SequentialColorMaps =['Blues', 'BuGn', 'BuPu', 'GnBu', 'Greens', 'Greys', 'Oranges', 'OrRd', 'PuBu', 'PuBuGn', 'PuRd', 'Purples', 'RdPu', 'Reds', 'YlGn', 'YlGnBu', 'YlOrBr', 'YlOrRd']
fig, ax = plt.subplots()
colormap = plt.cm.get_cmap(random.choice(SequentialColorMaps))
for idx,polygon in enumerate(polygons):
    ax.add_patch(patches.Polygon(polygon,
                         True,
                         edgecolor='none',
                         facecolor=colormap(random.random())[0:3]))
    ax.add_patch(patches.Circle(centerBC,
                                radiusBC,
                                edgecolor='k',
                                linewidth=0.1,
                                facecolor='none'))

print(polygons)
print(polygons.min())
print(polygons[:,:,0].min())
print(polygons[:,:,1].min())

print(polygons.max())
print(polygons[:,:,0].max())
print(polygons[:,:,1].max())

plt.axis('equal')
plt.show()